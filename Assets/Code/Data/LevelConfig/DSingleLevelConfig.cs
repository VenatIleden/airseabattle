﻿using System;

[Serializable]
public class DSingleLevelConfig 
{
    public int time_limit;

    public int default_high_score;

    public int points_per_plane;

    public string id;
}