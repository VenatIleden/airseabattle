﻿using UnityEngine;

[CreateAssetMenu(fileName = "LevelConfig", menuName = "CustomData/LevelConfig", order = 1)]
public class SingleLevelConfigSO : ScriptableObject
{
    public DSingleLevelConfig config;
}