﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "PlayerConfig", menuName = "CustomData/PlayerConfig", order = 1)]
public class DSinglePlayerConfig : ScriptableObject
{       
    [Header("Position")]

    public bool lookToTheLeft;
        
    public Sprite[] angleSprites;

    [Range(0,1)]
    public float horizontalPos = 0.25f;

    [Header("Controls")]

    public KeyCode turn_30;
    public KeyCode turn_90;
    public KeyCode shoot;
}
