﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UMainMenuCanvas : MonoBehaviour
{
    #region ATTRIBUTES

    [SerializeField]
    Text highScoreText;

    [Header("Last score")]

    [SerializeField]
    Text lastScoreTitle;

    [SerializeField]
    Text lastScoreText;

    #endregion

    #region METHODS

    /// <summary>
    /// Manages score texts and titles content and activation
    /// </summary>
    void Awake()
    {
        highScoreText.text = MGlobal.Instance.currentLevelConfig.config.default_high_score.ToString();

        if(MGlobal.Instance.lastScore != null)
        {
            lastScoreTitle.enabled = true;
            lastScoreText.enabled = true;
            lastScoreText.text = MGlobal.Instance.lastScore.ToString();
        }
    }

    public void BLaunchGameplayScene()
    {
        SceneManager.LoadScene(2);
    }

    #endregion
}