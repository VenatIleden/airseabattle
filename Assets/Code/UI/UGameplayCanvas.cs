﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UGameplayCanvas : Singleton<UGameplayCanvas>
{
    #region ATTRIBUTES

    [SerializeField]
    Text currentScoreText;

    [SerializeField]
    Text timeLeftText;

    [SerializeField]
    Text highScoreText;

    DSingleLevelConfig levelConfig;

    int timeLeft;

    int currentScore = 0;

    #endregion

    #region METHODS

    void Awake()
    {
        levelConfig = MGlobal.Instance.currentLevelConfig.config;        
    }

    void Start()
    {
        timeLeft = levelConfig.time_limit;

        timeLeftText.text = timeLeft.ToString();
        highScoreText.text = levelConfig.default_high_score.ToString();
        StartCoroutine(ModifyTimeLeft());
    }

    /// <summary>
    /// Calls itself every second to reduce main time left and check if the game is over
    /// </summary>
    IEnumerator ModifyTimeLeft()
    {
        yield return new WaitForSeconds(1);
        if (--timeLeft > 0)
        {                    
            timeLeftText.text = timeLeft.ToString();            
            StartCoroutine(ModifyTimeLeft());
        }
        else
        {
            MGlobal.Instance.lastScore = currentScore;
            if (currentScore > levelConfig.default_high_score)
            {
                MGlobal.Instance.currentLevelConfig.config.default_high_score = currentScore;
            }
            SceneManager.LoadScene(1);
        }            
    }

    public void ModifyCurrentScore(int increment)
    {
        currentScore += increment;
        currentScoreText.text = currentScore.ToString();
    }    

    #endregion
}