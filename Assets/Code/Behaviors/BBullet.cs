﻿using UnityEngine;

public class BBullet : BSpawnableItem
{
    #region ATTRIBUTES

    public Angle bulletAngle;
    public bool isLookingLeft;

    #endregion

    #region METHODS

    public void InitializeItemDirection()
    {        
        switch (bulletAngle)
        {
            case Angle.ThirtyDegrees:
            {
                if (isLookingLeft)
                {
                    itemDir = Vector3.Normalize(new Vector3(-78, 45, 0));
                }
                else
                {
                    itemDir = Vector3.Normalize(new Vector3(78, 45, 0));
                }
                break;
            }
            case Angle.SixtyDegrees:
            {
                if (isLookingLeft)
                {
                    itemDir = Vector3.Normalize(new Vector3(-45, 78, 0));
                }
                else
                {
                    itemDir = Vector3.Normalize(new Vector3(45, 78, 0));
                }
                break;
            }
            case Angle.NinetyDegrees:
            {
                itemDir = Vector3.up;
                break;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        itemsGenerator.ReturnItemToPool(gameObject);
    }

    public override void CheckItemOutOfScreen()
    {
        Vector3 itemPosition = transform.position;

        if (Mathf.Abs(itemPosition.x) > halvenScreenWidth || Mathf.Abs(itemPosition.y) > halvenScreenHeight)
        {
            itemsGenerator.ReturnItemToPool(gameObject);
        }
    }

    #endregion
}
