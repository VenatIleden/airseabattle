﻿using System.Collections;
using UnityEngine;

public class BEnemy : BSpawnableItem
{
    #region ATTRIBUTES
        
    AudioSource audioSource;
    SpriteRenderer spriteRenderer;
    BoxCollider2D boxCollider2D;

    #endregion

    #region METHODS

    protected override void Awake()
    {
        base.Awake();

        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider2D = GetComponent<BoxCollider2D>();
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(PlaneHit());
    }

    public void InitializeItemDirection()
    {
        itemDir = Vector3.right;
    }

    public override void CheckItemOutOfScreen()
    {
        Vector3 itemPosition = transform.position;

        if (itemPosition.x > halvenScreenWidth)
        {
            transform.position = new Vector3(-halvenScreenWidth, transform.position.y);
        }
    }

    /// <summary>
    /// Switches the sprite renderer and box collider enabled states
    /// </summary>
    public void ToggleComponents()
    {
        spriteRenderer.enabled = !spriteRenderer.enabled;
        boxCollider2D.enabled = !boxCollider2D.enabled;
    }

    /// <summary>
    /// Manages all functionality related to the impact event: hiding plane, playing audio
    /// </summary>
    /// <returns></returns>
    IEnumerator PlaneHit()
    {
        ToggleComponents();
        UGameplayCanvas.Instance.ModifyCurrentScore(MGlobal.Instance.currentLevelConfig.config.points_per_plane);
        audioSource.Play();

        yield return new WaitForSeconds(audioSource.clip.length);
        itemsGenerator.ReturnItemToPool(gameObject);
    }    

    #endregion
}
