﻿using System;
using UnityEngine;

public class BPlayer : BSpawnableItem
{
    #region ATTRIBUTES
    
    [SerializeField]
    DSinglePlayerConfig playerConfig;

    MBullets bulletsManager;

    Angle currentAngle;

    Angle CurrentAngle
    {
        get { return currentAngle; }
        set
        {
            currentAngle = value;

            spriteRenderer.sprite = playerConfig.angleSprites[(byte)currentAngle];
        }
    }

    SpriteRenderer spriteRenderer;

    #endregion

    #region METHODS

    protected override void Awake()
    {
        base.Awake();
        spriteRenderer = GetComponent<SpriteRenderer>();
        bulletsManager = FindObjectOfType<MBullets>();
    }
    
    void Start()
    {        
        if (playerConfig.lookToTheLeft)
        {
            spriteRenderer.flipX = true;
        }

        float xPos = -halvenScreenWidth + (2 * halvenScreenWidth * playerConfig.horizontalPos);
        Vector3 startPos = new Vector3(xPos, -halvenScreenHeight);
        SpawnItem(startPos);
    }

    void Update()
    {
        if (Input.GetKeyDown(playerConfig.turn_30))
        {
            CurrentAngle = Angle.ThirtyDegrees;
        }
        else if (Input.GetKeyDown(playerConfig.turn_90))
        {
            CurrentAngle = Angle.NinetyDegrees;
        }
        else if ((Input.GetKeyUp(playerConfig.turn_30) &&  CurrentAngle == Angle.ThirtyDegrees) || (Input.GetKeyUp(playerConfig.turn_90) && CurrentAngle == Angle.NinetyDegrees))
        {
            CurrentAngle = Angle.SixtyDegrees;
        }
        if (Input.GetKeyDown(playerConfig.shoot))
        {
            bulletsManager.ShootNewBullet(transform.position, playerConfig.lookToTheLeft, CurrentAngle);            
        }
    }

    public override void SpawnItem(Vector3 spawnPoint)
    {
        base.SpawnItem(spawnPoint);
    }

    #endregion
}