﻿using UnityEngine;

public class BSpawnableItem : MonoBehaviour
{
    #region ATTRIBUTES

    public Vector3 itemDir;
    public BItemsGenerator itemsGenerator;

    protected float halvenScreenWidth;
    protected float halvenScreenHeight;

    #endregion

    #region METHODS

    protected virtual void Awake()
    {
        halvenScreenWidth = Camera.main.orthographicSize * Camera.main.aspect;
        halvenScreenHeight = Camera.main.orthographicSize;
    }

    public virtual void SpawnItem(Vector3 spawnPoint)
    {
        transform.position = spawnPoint;
    }

    public virtual void CheckItemOutOfScreen() { }

    #endregion
}
