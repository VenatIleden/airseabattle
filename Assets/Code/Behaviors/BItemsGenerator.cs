﻿using System.Collections.Generic;
using UnityEngine;

public class BItemsGenerator : MonoBehaviour
{
    #region ATTRIBUTES

    public GameObject itemToSpawn;    

    public float itemSpeed;

    protected List<BSpawnableItem> instantiatedItems = new List<BSpawnableItem>();

    protected Queue<GameObject> availableItemsPool = new Queue<GameObject>();

    #endregion

    #region METHODS

    protected virtual void Update()
    {
        for (byte i = 0; i < instantiatedItems.Count; ++i)
        {
            instantiatedItems[i].gameObject.transform.Translate(instantiatedItems[i].itemDir * Time.deltaTime * itemSpeed);

            instantiatedItems[i].CheckItemOutOfScreen();
        }
    }
        
    /// <summary>
    /// Call this method whenever an item should disappear from scene and get ready
    /// for future recycling
    /// </summary>
    /// <param name="itemObj">GameObject attached to the item info</param>
    public virtual void ReturnItemToPool(GameObject itemObj)
    {
        for (byte a = 0; a < instantiatedItems.Count; ++a)
        {
            if (itemObj == instantiatedItems[a].gameObject)
            {
                instantiatedItems.Remove(instantiatedItems[a]);
                itemObj.SetActive(false);
                availableItemsPool.Enqueue(itemObj);
                break;
            }
        }
    }
    
    protected BSpawnableItem GenerateNewItem()
    {        
        GameObject newItemObj;

        if (availableItemsPool.Count > 0)
        {
            newItemObj = availableItemsPool.Dequeue();
        }
        else
        {
            newItemObj = Instantiate(itemToSpawn);
        }

        newItemObj.SetActive(true);

        return newItemObj.GetComponent<BSpawnableItem>();
    }

    #endregion
}