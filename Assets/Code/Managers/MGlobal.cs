﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class MGlobal : Singleton<MGlobal>
{
    #region ATTRIBUTES

    public SingleLevelConfigSO currentLevelConfig;

    [Tooltip("This configuration will only be used if conection fails or there's no remote data")]
    [SerializeField]
    SingleLevelConfigSO defaultLevelConfig;

    [Tooltip("Web page where level config data is stored")]
    [SerializeField]
    string remoteConfigPath;

    public int? lastScore;

    #endregion

    #region METHODS

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    IEnumerator Start()
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(remoteConfigPath);

        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError)
        {
            currentLevelConfig.config = defaultLevelConfig.config;
        }
        else
        {
            currentLevelConfig.config = JsonUtility.FromJson<DSingleLevelConfig>(webRequest.downloadHandler.text);            
        }

        SceneManager.LoadScene(1);
    }

    #endregion
}

/// <summary>
/// Three possible angles state for both stationary gun and bullets
/// </summary>
public enum Angle : byte
{
    SixtyDegrees,
    ThirtyDegrees,
    NinetyDegrees
}