﻿using UnityEngine;

public class MBullets : BItemsGenerator
{
    #region ATTRIBUTES
    
    [SerializeField]
    byte maxBullets;

    #endregion

    #region METHODS
        
    protected override void Update()
    {
        base.Update();
    }    

    public void ShootNewBullet(Vector3 gunPosition, bool isLookingLeft, Angle gunAngle)
    {
        if(instantiatedItems.Count < maxBullets)
        {           
            BBullet newItemData = (BBullet)GenerateNewItem();
            newItemData.itemsGenerator = this;

            newItemData.bulletAngle = gunAngle;
            newItemData.isLookingLeft = isLookingLeft;
            newItemData.InitializeItemDirection();
            newItemData.SpawnItem(gunPosition);

            instantiatedItems.Add(newItemData);
        }
    } 

    #endregion
}