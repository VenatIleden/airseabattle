﻿using System.Collections;
using UnityEngine;

public class MEnemies : BItemsGenerator
{
    #region ATTRIBUTES
    
    [SerializeField]
    [Range(0, 0.9f)]
    float topSpawnLimit;

    [SerializeField]
    [Range(-0.9f, 0)]
    float BottomSpawnLimit;
    
    float horizontalLimit;

    int currentWaveSize;

    #endregion

    #region METHODS

    void Awake()
    {
        horizontalLimit = Camera.main.orthographicSize * Camera.main.aspect;
    }

    void Start()
    {
        StartCoroutine(LaunchNewWave());
    }

    protected override void Update()
    {
        base.Update();
    }
    
    /// <summary>
    /// After a small delay, calculates a new wave size and generates all required items
    /// </summary>
    /// <returns></returns>
    IEnumerator LaunchNewWave()
    {
        yield return new WaitForSeconds(0.5f);
        
        currentWaveSize = Random.Range(3, 6);

        for(byte c = 0; c < currentWaveSize; ++c)
        {            

            BEnemy newItemData = (BEnemy)GenerateNewItem();
            newItemData.itemsGenerator = this;

            float planeHeight = BottomSpawnLimit * Camera.main.orthographicSize + ((topSpawnLimit - BottomSpawnLimit) * Camera.main.orthographicSize / currentWaveSize) * c;

            newItemData.SpawnItem(new Vector3(-horizontalLimit, planeHeight));
                                    
            newItemData.ToggleComponents();
            newItemData.InitializeItemDirection();

            instantiatedItems.Add(newItemData);
        }
    }

    public override void ReturnItemToPool(GameObject itemObj)
    {
        base.ReturnItemToPool(itemObj);

        if (instantiatedItems.Count == 0)
        {
            StartCoroutine(LaunchNewWave());
        }
    }

    #endregion
}
